var logic = require("./logic/main");

module.exports.trigger = async (event, context) => {
  var data = JSON.parse(event.body)
  if (data.message.chat.type !== "private") return {
    statusCode: 200,
    body: "ok"
  }
  var message = data.message.text
  var portal_id = data.message.chat.id
  var portal = "telegram"
  console.log(portal_id, message)
  try {
    await logic.trigger(portal, portal_id, message)
  } catch (e) {}
  return {
    statusCode: 200,
    body: "ok"
  }
  // Use this code if you don't use the http event with the LAMBDA-PROXY integration
  // callback(null, { message: 'Go Serverless v1.0! Your function executed successfully!', event });
};
