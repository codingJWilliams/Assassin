const Telegram = require('telegraf/telegram')
const axios = require("axios")
const bot = new Telegram(process.env.TELEGRAM_KEY)

module.exports = async (portal, portal_id, content) => {
    return await axios.get(`https://api.telegram.org/bot${process.env.TELEGRAM_KEY}/sendMessage?chat_id=${encodeURIComponent(portal_id)}&text=${encodeURIComponent(content)}&parse_mode=Markdown`)
}
