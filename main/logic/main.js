const fs = require("fs");
const oauth = "https://t.co/imiGXTvh3I";
var Gist = require("gist.js");
var jwt = require("jsonwebtoken");
const { promisify } = require("util");
var knex = require('knex')({
    client: 'mysql',
    connection: {
        host: process.env.MYSQL_HOST,
        port: 3336,
        user: "root",
        password: process.env.MYSQL_PASSWORD,
        database: "assassin"
    }});
var util = require("./util")

function getGist(id) {
    return new Promise((resolve, reject) => {
        var gist = Gist(id);
        gist.get(function (err, json) {
            if (err) return reject(err);
            resolve(json)
        })
    })
}

module.exports.trigger = async (portal, portal_id, message) => {
    async function send(portal, portal_id, content) {
        console.log(portal, portal_id, content)
        await require("../" + portal + "_sender.js")(
            portal, 
            portal_id, 
            content)
    }
    async function reply(content) {
        await send(portal, portal_id, content)
    }
    // Deal with authentication requests
    if (message.startsWith("t=")) {
        if (await knex.select().from("people").where({
            portal: portal,
            portal_id: portal_id
        }).first()) { // Check if a row exists for this chat portal
            return await reply("You're already authenticated in this chat! Type `logout` to log out.")
        }
        var gistId = message.substring(2);
        var json = await getGist(gistId)
        var claim = json.files["i_am.jwt"].content;
        try {
            var a = jwt.verify(claim, process.env.JWT_KEY);
            if (a.hd !== "nbacademy.org.uk") return reply("That google account isn't an nbacademy.org.uk account. Please ensure you're logging in with your school account. If you're not presented with a login screen just go to google.com and click on your profile at the top right, click 'Add account' and log in with your nba account. This should allow you to choose to use your school account to login.")
            if (await knex.select().from("people").where({email: a.email}).first()) { 
                // If their email is already authenticated elsewhere
                await knex("people")
                  .where({ email: a.email })
                  .update({
                    portal: portal,
                    portal_id: portal_id
                  });
                return await reply("Coolio! You were already authenticated elsewhere so I've disconnected that.")
            }
            // Email is new
            await knex("people")
              .insert({
                name: a.name,
                email: a.email,
                game: null,
                portal: portal,
                portal_id: portal_id
              })
            return await reply("Great! I've logged you in as " + a.name)

        } catch (e) {
            return await reply("That token was invalid, please try again or contact Jay")
        }
    }
    // user isn't requesting to auth


    var person = await knex("people").where({ portal:portal, portal_id:portal_id }).first();
    // check if user is authed
    if (!person) {
        // user is not authed
        return await reply("Hey there! You're not authenticated - just follow the instructions on this webpage (and make sure to use a school google account): " + oauth)
    }


    // Help command
    if (message.startsWith("help")) return await reply(`Hey there! 
        
To start a game type \`create <code>\`, with \`<code>\` being any word. 
To join someone else's game just type \`join <theircode>\`. 
You can only be in one game at a time. When the game creator has chosen all of the parameters they can type \`start\` to assign all missions.
In game you can type \`mymission\` to see your mission, and \`assassinated\` to show you've done your mission.`)
    
    // Create game
    if (message.startsWith("create ")) {
        if (person.game) return await reply("You're currently in a game. Please leave or destroy this first.")
        var code = message.split(" ")[1];
        var existingGame = await knex("games").where({game_code: code}).first();
        if (existingGame) return await reply("This game already exists! Perhaps choose another code.")
        
        await knex("games").insert({
            game_code: code,
            admin: person.email,
            started: 0
        })
        await knex("people")
            .where({
                email: person.email
            })
            .update({
                game: code
            })
        return await reply("I've made that game! Players can join with `join " + code + "`. You are the game admin and can start the game when enough players have joined; you do this by typing `start`. You are inherently joined to the game as the admin and you cannot leave, however you may `destroy` the game, disconnecting all players.")
    }
    if (message.startsWith("join ")) {
        var code = message.split(" ")[1];
        if (person.game) return await reply("You're currently in a game. Please leave or destroy this first.")
        var game = await knex("games").where({game_code: code}).first();
        if (!game) return await reply("I can't find that game; the code is case-sensitive so maybe try again")
        if (game.started) return await reply("You can't join that game; it has already started")
        // Game is in lobby, game exists, person is not in a game
        await knex("people")
          .where({ email: person.email })
          .update({ game: game.game_code })
        var admin = await knex("people").where({email: game.admin}).first();
        if (!admin) return await reply("huh I couldn't find the admin.... this should never happen please tell jay")
        await send(admin.portal, admin.portal_id, `${person.email} (${person.name}) just joined the game!`)
        return await reply("Great! I've joined you to the game. When the admin starts the game you will recieve your mission through this DM!")
    }
    if (message == "start") {
        if (!person.game) return await reply("You're not in a game");
        var game = await knex("games")
          .where({ game_code: person.game})
          .first()
        if (game.admin !== person.email) return await reply("You're not the admin in this game :(")
        if (game.started) return await reply("You've already started this game >.<")
        var players = await knex("people")
            .where({ game: game.game_code })
            .select()
        if (players.length < 2) return await reply("You don't have enough people to start this game")
        var objects = require("../stuff/objects.json");
        var places = require("../stuff/places.json");
        var least = (objects.length > places.length) ? places.length : objects.length;
        if (players.length > least) return await reply("There are too many players! The maximum is " + least)
        var unpickedPeople = JSON.parse(JSON.stringify(players));
        for (var i = 0; i < players.length; i++) {
            var pickedPerson = unpickedPeople.pop();
            if (pickedPerson.email == players[i].email) {
                unpickedPeople = [pickedPerson].concat(unpickedPeople);
                pickedPerson = unpickedPeople.pop()
            }
            players[i].target = pickedPerson;
            players[i].object = objects.pop();
            players[i].location = places.pop();
            await send(players[i].portal, players[i].portal_id, `The game has started!
You have to give \`${players[i].target.name} ${players[i].object} ${players[i].location}\`.

When you've completed your mission just type \`assassinated\` to be given your new mission.`)
            await knex("playing_players").insert({
                email: players[i].email,
                target_email: players[i].target_email,
                location: players[i].location,
                object: players[i].object,
                still_in: 1
            })
            await knex("game_scores").insert({
                email: players[i].email,
                game_code: game.game_code,
                points: 0
            })
        }
        if (message == "assassinated") {
            var mission_etc = await knex("playing_players")
                .where({email: person.email})
                .first();
            if (!mission_etc) return await reply("You're not currently in a game.")
            var target = await knex("people").where({email: mission_etc.email}).first();
            var target_mission = await knex("playing_players").where({email: mission_etc.email}).first();
            var target_score = await knex("game_scores").where({email: target.email}).first();
            await send(target.portal, target.portal_id, `You were assassinated by ${person.name} :(. You ended the game on ${target_score.points} points. I've removed you from the game; you are free to join another.`)
            await knex("playing_players").where({email: target.email}).delete()
            await knex("game_scores").where({email: target.email}).delete()
            await knex("people").where({email: target.email}).update({game: null})
            await knex("game_scores").where({email: person.email}).increment("points", 1)
            var new_target = await knex("people").where({email: target_mission.target_email}).first();
            await reply("Good job! You've gained a point. Your new mission, should you choose to accept it, is to assassinate " + `\`${new_target.name} with ${target_mission.object} ${target_mission.location}\`. Good luck.`)
        }
        await knex("games")
          .where({ game_code: game.game_code })
          .update({started: 1})
        return await reply("I've started the game! " + players.length + " players have been notified.")
    }
    return await reply("I don't recognise that command. Please see `help`")
}