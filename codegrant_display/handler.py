import json, base64, os, jwt
from botocore.vendored import requests
cid = os.environ["GOOGLE_CLIENT_ID"]
cs = os.environ["GOOGLE_CLIENT_SECRET"]
redir = os.environ["GOOGLE_REDIRECT"]
github_auth = os.environ["GITHUB_AUTH"]
pkey = os.environ["JWT_KEY"]

def hello(event, context):
    body = {
        "message": "Go Serverless v1.0! Your function executed successfully!",
        "input": event
    }
    r = requests.post("https://www.googleapis.com/oauth2/v4/token", 
        data="client_id=" + cid + "&client_secret=" + cs + "&code=" + event["queryStringParameters"]["code"] + "&redirect_uri=" + redir + "&grant_type=authorization_code",
        headers={
            "Content-Type": "application/x-www-form-urlencoded"
        }
    )
    r = r.json()
    uInfo = requests.get("https://www.googleapis.com/oauth2/v1/userinfo?alt=json", headers={
        "Authorization": "Bearer " + r["access_token"]
    }).json()
    j = jwt.encode(uInfo, pkey).decode("utf8")
    r = requests.post("https://" + github_auth + "@api.github.com/gists", json={
        "files": {
            "i_am.jwt": {
                "content": j
            }
        },
        "description": "Identity proof"
    }).json()
    with open("template.html") as f: out = f.read().replace("%token%", "t=" + r["id"])
    return {
        "headers": { "Access-Control-Allow-Origin": "'*'", "Content-Type": "text/html"},
        "statusCode": 200,
        "body": out
    }
